package rcclpmo.com.gaviewer.callbacks;


import android.graphics.Bitmap;

public interface VolleyCallback {
    void onSuccessResponse(String result);
    void onErrorResponse(String result);

}

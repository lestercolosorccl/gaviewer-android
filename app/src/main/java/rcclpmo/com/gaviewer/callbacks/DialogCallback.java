package rcclpmo.com.gaviewer.callbacks;

public interface DialogCallback {
    void onSuccessResponse(boolean result);
}

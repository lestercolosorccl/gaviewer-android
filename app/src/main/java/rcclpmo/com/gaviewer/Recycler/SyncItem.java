package rcclpmo.com.gaviewer.Recycler;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by 7567 on 22/03/2018.
 */

public class SyncItem{

    private String title;
    private Integer download_count = 0;
    private Integer count;
    private boolean isSelected;
    private boolean isDone;


    public SyncItem() {

    }

    public SyncItem(String title, Integer count) {
        this.title = title;
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public Integer getCount() {
        return count;
    }

    public void setTitle(String name) {
        this.title = name;
    }


    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean getDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Integer getDownloadCount() {
        return download_count;
    }

    public void addDownloadCount(){
        download_count = download_count+1;
    }

    public void setDownloadCount(Integer num) {
        download_count = num;
    }



}

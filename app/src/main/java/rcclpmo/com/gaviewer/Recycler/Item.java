package rcclpmo.com.gaviewer.Recycler;

/**
 * Created by 7567 on 12/03/2018.
 */

public class Item {

    private String title, status;

    public Item() {
    }

    public Item(String title, String status) {
        this.title = title;
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }


    public String getStatus() {
        return status;
    }

    public void setGenre(String status) {
        this.status = status;
    }
}

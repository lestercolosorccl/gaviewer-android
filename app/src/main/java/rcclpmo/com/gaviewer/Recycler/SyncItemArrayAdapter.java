package rcclpmo.com.gaviewer.Recycler;

import android.content.Context;
import android.graphics.ColorSpace;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rcclpmo.com.gaviewer.R;

public class SyncItemArrayAdapter extends RecyclerView.Adapter<SyncItemArrayAdapter.MyViewHolder> {

    private List<SyncItem> SyncItemList;
    private ArrayList<Boolean> listactive = new ArrayList<Boolean>();

    private LayoutInflater inflater;
    public static ArrayList<ColorSpace.Model> imageModelArrayList;
    private Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout syncContainer;
        public TextView title;
        public TextView progressCount;
        public CheckBox checkbox;
        public ProgressBar progressimg;
        public ImageView imgdone;

        public MyViewHolder(View view) {
            super(view);
            syncContainer = (LinearLayout) view.findViewById(R.id.syncContainer);
            title = (TextView) view.findViewById(R.id.item_title);
            progressCount = (TextView) view.findViewById(R.id.progressCount);
            checkbox = (CheckBox) view.findViewById(R.id.item_checkbox);
            progressimg = (ProgressBar) view.findViewById(R.id.progressimg);
            imgdone = (ImageView) view.findViewById(R.id.imgdone);

//            progressimg.setProgress(100000);
            this.setIsRecyclable(false);
        }
    }



    public SyncItemArrayAdapter(List<SyncItem> SyncItemList) {
        this.SyncItemList = SyncItemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sync_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SyncItem SyncItem = SyncItemList.get(position);


        if(SyncItem.getCount() != null ){
            holder.syncContainer.setVisibility(View.VISIBLE);
            holder.title.setText(SyncItem.getTitle()+" - ");
            holder.progressCount.setText(SyncItem.getDownloadCount()+"/"+SyncItem.getCount());

            if(SyncItemList.get(position).getDone()){
                holder.imgdone.setVisibility(View.VISIBLE);
                holder.progressimg.setVisibility(View.GONE);
            }else{
                holder.imgdone.setVisibility(View.GONE);
                holder.progressimg.setVisibility(View.VISIBLE);
            }

        }else{
            holder.checkbox.setVisibility(View.VISIBLE);
            holder.syncContainer.setVisibility(View.GONE);
            holder.checkbox.setText(SyncItem.getTitle());
            holder.checkbox.setChecked(SyncItemList.get(position).getSelected());
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SyncItemList.get(position).getSelected()){
                        SyncItemList.get(position).setSelected(false);
                    }else {
                        SyncItemList.get(position).setSelected(true);
                    }
                }
            });
        }
    }

    public ArrayList getListActive(){
        listactive = new ArrayList<Boolean>();
        for (int i = 0; i < SyncItemList.size(); i++) {
            listactive.add(getActive(i));
        }
        return listactive;
    }

    public Integer getActiveCount(){
        Integer result = 0;
        for (int i = 0; i < SyncItemList.size(); i++) {
            if(getActive(i)){
                result++;
            }
        }
    return result;
    }

    public void addDownloadCount(Integer pos){
        SyncItemList.get(pos).addDownloadCount();
    }
    public Integer getDownloadCount(Integer pos){
        return SyncItemList.get(pos).getDownloadCount();
    }

    public Boolean getActive(Integer pos) {
        return SyncItemList.get(pos).getSelected();
    }

    public void setDone(Integer pos){
        SyncItemList.get(pos).setDone(true);
    }

    public void setDownloadCount(Integer pos, Integer num){
        SyncItemList.get(pos).setDownloadCount(num);
    }

    @Override
    public int getItemCount() {
        return SyncItemList.size();
    }

}
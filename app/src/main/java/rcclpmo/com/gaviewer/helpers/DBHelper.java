package rcclpmo.com.gaviewer.helpers;


import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "gaviewer.db";

    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        db.execSQL("DROP TABLE login");
        db.execSQL("create table offline (sync_data text,  sync_date date default CURRENT_DATE)");
        db.execSQL("create table login (login_data text,  login_date date default CURRENT_DATE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS login");
        db.execSQL("DROP TABLE IF EXISTS offline");
        onCreate(db);
    }

    public boolean insertLogin(JSONObject data) {

        deleteLogin();
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE login");
        db.execSQL("create table login (login_data text,  login_date date default CURRENT_DATE)");

        ContentValues contentValues = new ContentValues();
        contentValues.put("login_data", String.valueOf(data));
        String result = String.valueOf(db.insert("login", null, contentValues));
        return true;
    }

    public boolean insertSync(JSONArray data) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS offline");
        db.execSQL("create table offline (sync_data text,  sync_date date default CURRENT_DATE)");
        ContentValues contentValues = new ContentValues();
        contentValues.put("sync_data", String.valueOf(data));
        String result = String.valueOf(db.insert("offline", null, contentValues));
        return true;
    }


    public String getSync() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from offline", null );
        res.moveToFirst();
        String data = res.getString(0);
        return data;
    }

    public boolean checkSync(){
        boolean result = false;
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "offline");
        if(numRows>=1){
            result = true;
        }
        return result;
    }

    public String getLogin() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from login", null );
        res.moveToFirst();
        String data = res.getString(0);
        return data;
    }

    public boolean checkLogin(){
        boolean result = false;
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "login");
        if(numRows>=1){
            result = true;
        }
        return result;
    }

    public Integer deleteLogin () {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("login", null, null);
    }

    public ArrayList<String> getUserData() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from login", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex("login")));
            res.moveToNext();
        }
        return array_list;
    }


}
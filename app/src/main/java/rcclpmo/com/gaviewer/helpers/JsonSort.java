package rcclpmo.com.gaviewer.helpers;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created by 7567 on 02/04/2018.
 */

public class JsonSort {

    /**
     * Sorts the given JSONObject
     *
     * @param jsonObject
     * @return jsonObject sorted in descending order
     */
    public JSONObject sortJSONObject(JSONObject jsonObject) throws JSONException {

        ArrayList<SomeChildClass> alstSomeChildClass = new ArrayList<>();

        Iterator<String> keys = jsonObject.keys();

        while (keys.hasNext()) {
            SomeChildClass someChildClass = new SomeChildClass();
            someChildClass.key = keys.next();
            someChildClass.value = jsonObject.getString(someChildClass.key);
            alstSomeChildClass.add(someChildClass);
        }

        // Sort the Collection
        Collections.sort(alstSomeChildClass);

        JSONObject sortedJsonObject = new JSONObject();
        for (SomeChildClass someChildClass : alstSomeChildClass)
            sortedJsonObject.put(someChildClass.key, someChildClass.value);

        return sortedJsonObject;
    }

    private class SomeChildClass implements Comparable<SomeChildClass> {

        private String key;
        private String value;

        @Override
        public int compareTo(@NonNull SomeChildClass target) {

            int currentVal = Integer.parseInt(value);
            int targetVal = Integer.parseInt(target.value);

            return (currentVal < targetVal) ? 1 : ((currentVal == targetVal) ? 0 : -1);
        }
    }
}

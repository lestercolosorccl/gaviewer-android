package rcclpmo.com.gaviewer.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.app.Application;

import com.android.volley.Response;

import org.json.JSONException;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import rcclpmo.com.gaviewer.R;
import rcclpmo.com.gaviewer.callbacks.DialogCallback;

/**
 * Created by 7567 on 06/02/2018.
 */

public class GlobalHelper extends Application{
//    public static String ServerConn = "https://dev.rcclpmo.com/";
    private Context context;
    private static GlobalHelper instance;
    private LayoutInflater layoutInflater;
    AlertDialog alert11;

    public static GlobalHelper getInstance() {
        return instance;
    }
    private static Toast mToast;

    public GlobalHelper(Context cntxt) {
        context = cntxt;
    }

    public Dialog dialogloader = null;

    public String SetLoaderText = "Loading...";

    public boolean isOnline() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else {
            connected = false;
//            Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
        }
        return connected;
    }

    public void Alert(String contents){

        AlertDialog.Builder Alert1 = new AlertDialog.Builder(context);
        Alert1.setMessage(contents);
        Alert1.setCancelable(true);

        Alert1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = Alert1.create();
        alert11.show();
    }


    public Object Confirm(final String contents, final DialogCallback callback) throws JSONException {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View content = inflater.inflate(R.layout.confirm_layout, null);
        builder1.setView(content);
        builder1.setCancelable(true);

        TextView tvConfirmTitle = (TextView) content.findViewById(R.id.tvConfirmTitle);
        TextView tvConfirmContent = (TextView) content.findViewById(R.id.tvConfirmContent);
        Button btnConfirmNo = (Button) content.findViewById(R.id.btnConfirmNo);
        Button btnConfirmYes = (Button) content.findViewById(R.id.btnConfirmYes);
        tvConfirmTitle.setText("Logout");
        tvConfirmContent.setText(contents);

        btnConfirmNo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                callback.onSuccessResponse(false);
                alert11.cancel();
            }
        });

        btnConfirmYes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                callback.onSuccessResponse(true);
                alert11.cancel();
            }
        });
        alert11 = builder1.create();
        alert11.show();
        return false;
    }



    public void Toast(String contents){
        KillToast();
        int duration = Toast.LENGTH_SHORT;
        mToast = Toast.makeText(context, contents, duration);
        mToast.show();
    }

    public void KillToast(){
        if (mToast != null) mToast.cancel();
    }

    public String ConvertToDate(String date,String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern); // here set the pattern as you date in string was containing like date/month/year
        String d = null;
        try {
            d = String.valueOf(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public void Loader(Boolean show){
        if (dialogloader != null) dialogloader.cancel();

        dialogloader = new Dialog(context, R.style.Loader);
        dialogloader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogloader.setCancelable(false);
        dialogloader.setContentView(R.layout.loader);
        dialogloader.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#80000000")));
        TextView loadertext = dialogloader.findViewById(R.id.loadertext);
        loadertext.setText(SetLoaderText);
        dialogloader.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);


        if(show){
            dialogloader.show();
        }else{
            dialogloader.cancel();
            dialogloader = null;
        }
    }

    public boolean is_Loader(){
        Boolean result = false;
        if(dialogloader != null){
            result = true;
        }
        return result;
    }

    public void LoaderWithCancel(final DialogCallback callback){
        Loader(true);
        dialogloader.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    callback.onSuccessResponse(true);
                    dialogloader.cancel();
                }
                return true;
            }
        });
    }


    public String parseDateToddMMyyyy(String time, String inputPattern, String outputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
                    encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public void deleteFolder(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteFolder(child);
            }
        }

        fileOrDirectory.delete();
    }

    public boolean checkFile(String filepath){
        File file = new File(filepath);
        return  file.exists();
    }

    public void log(String content){
        Log.v("lester-log", content);
    }


}

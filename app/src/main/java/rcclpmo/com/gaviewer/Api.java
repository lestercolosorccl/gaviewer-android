package rcclpmo.com.gaviewer;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;


import rcclpmo.com.gaviewer.callbacks.VolleyCallback;

public class Api extends Application {

    public static String ServerURL = BuildConfig.SERVER_URL;
    public static String ImageURL = "https://s3-us-west-2.amazonaws.com/nb-gaviewer/";
    public static String accessToken = "d4d1bebc4185a97033c34d880f679511775ef531ca294f3c8b17a0b9e45634";
    private static final String TAG = "API";
    File directory;



    public static Object Login(final String email, final String password, final VolleyCallback callback) throws JSONException {

        String url =  ServerURL+"api/mobile/Auth/login";
        String data = null;

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccessResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                JSONObject response = new JSONObject();
                try {
                    response.put("success", false);
                    response.put("message", "Something went wrong.");
                    Log.v("lester-api", "failed");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.onErrorResponse(String.valueOf(response));
            }
        }) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> postparams = new HashMap<String, String>();
                postparams.put("email", email);
                postparams.put("password", password);
                return postparams;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }

    public static void CancelCall(){
        MyApplication.getInstance().cancelAllRequests(TAG);
    }


    public static Object Ship(final String id, final String access_token, final VolleyCallback callback) throws JSONException {
        String url =  ServerURL+"api/mobile/GA_ViewerMobile/getShipsDisciplinesDrawingsList";
        String data = null;

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccessResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                JSONObject response = new JSONObject();
                try {
                    response.put("success", false);
                    response.put("message", "Please check your internet connection.");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.onErrorResponse(String.valueOf(response));
            }
        }) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> postparams = new HashMap<String, String>();
                postparams.put("id", id);
                postparams.put("access_token", access_token);
                postparams.put("brandname", "android");
                return postparams;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }

    public static void DownloadImage(final String filename, final File directory, final VolleyCallback callback) throws JSONException {

            ImageRequest request = new ImageRequest(ImageURL+filename,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        String fname = saveImg(bitmap,filename, directory);
                        callback.onSuccessResponse(fname);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        JSONObject response = new JSONObject();
                        try {
                            response.put("success", false);
                            response.put("message", "Please check your internet connection.");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        callback.onErrorResponse(String.valueOf(response));
                    }
                });
        MyApplication.getInstance().addToRequestQueue(request, TAG);
    }


    public static String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }



    private static String saveImg(Bitmap bitmapImage, String fname, File directory){
        File mypath=new File(directory,fname);
        FileOutputStream fos = null;
        try {
            // fos = openFileOutput(filename, Context.MODE_PRIVATE);
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath()+"/"+fname;
    }



}


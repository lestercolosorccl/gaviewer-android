package rcclpmo.com.gaviewer.controllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rcclpmo.com.gaviewer.Api;
import rcclpmo.com.gaviewer.Recycler.Item;
import rcclpmo.com.gaviewer.Recycler.ItemArrayAdapter;
import rcclpmo.com.gaviewer.Recycler.RecyclerItemClickListener;
import rcclpmo.com.gaviewer.Recycler.SyncItem;
import rcclpmo.com.gaviewer.Recycler.SyncItemArrayAdapter;
import rcclpmo.com.gaviewer.callbacks.DialogCallback;
import rcclpmo.com.gaviewer.callbacks.VolleyCallback;
import rcclpmo.com.gaviewer.helpers.DBHelper;
import rcclpmo.com.gaviewer.R;
import rcclpmo.com.gaviewer.helpers.GlobalHelper;
import rcclpmo.com.gaviewer.helpers.JsonSort;
import uk.co.senab.photoview.PhotoViewAttacher;

public class MainActivity extends BaseActivity {

    ImageView imgLogout;
    ImageView imgSync;
    ImageView imgIsOnline;

    private DBHelper mydb;
    private GlobalHelper global;

    private TextView tvShip;
    private TextView tvDiscipline;
    private TextView tvDrawing;

    private LinearLayout LnrDiscipline;
    private LinearLayout LnrDrawing;
    private LinearLayout LnrDrawingLogoContainer;
    private HorizontalScrollView ImageContainer;

    private RelativeLayout mRelativeLayout;

    private PopupWindow mPopupWindow;

    private String access_token;
    private JSONObject obj_user;
    private String user_id;

    private JSONArray sync_shipdata;
    private JSONArray shipdata;
    private JSONArray disciplinedata;
    private JSONArray drawingdata;

    private List<Item> itemList = new ArrayList<>();
    private List<SyncItem> SyncItemList = new ArrayList<SyncItem>();
    private RecyclerView recyclerView;
    private ItemArrayAdapter iAdapter;
    private SyncItemArrayAdapter iSyncAdapter;

    AlertDialog dialog;
    AlertDialog sync_dialog;
    private Integer selectedShip;
    private Integer selectedDiscipline;
    private Integer selectedDrawing;

    ImageView DrawingView;
    ImageView imgDrawingLogo;

    View Recycler_content = null;

    File directory;

    ContextWrapper cw;

    int phone_height;

    Boolean is_sync = false;

    Button btnSyncCancel;
    Button btnSync;

    Button btnOptionCancel;

    ArrayList SyncShipArray = new ArrayList();
    ArrayList SyncDrawingData = new ArrayList();

    Integer SyncCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mydb = new DBHelper(this);
        global = new GlobalHelper(this);

        setContentView(R.layout.activity_main);

        getUser();

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }

        imgLogout = (ImageView) findViewById(R.id.imgLogout);
        imgSync = (ImageView) findViewById(R.id.imgSync);
        imgIsOnline = (ImageView) findViewById(R.id.imgIsOnline);

        tvShip = (TextView) findViewById(R.id.tvShip);

        LnrDiscipline = (LinearLayout) findViewById(R.id.LnrDiscipline);
        LnrDrawing = (LinearLayout) findViewById(R.id.LnrDrawing);
        LnrDrawingLogoContainer = (LinearLayout) findViewById(R.id.LnrDrawingLogoContainer);
        ImageContainer = (HorizontalScrollView) findViewById(R.id.ImageContainer);


        tvDiscipline = (TextView) findViewById(R.id.tvDiscipline);
        tvDrawing = (TextView) findViewById(R.id.tvDrawing);

        DrawingView = (ImageView) findViewById(R.id.imgDrawing);
        imgDrawingLogo = (ImageView) findViewById(R.id.imgDrawingLogo);

        //set image path
        cw = new ContextWrapper(this);
        directory = cw.getDir("gaviewer", Context.MODE_PRIVATE);

        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(DrawingView);
        pAttacher.update();

        getShip("default");
        setListeners();
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        phone_height = (int) (size.y*2.5);

    }


    private BroadcastReceiver networkStateReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            if(ni == null){
                imgIsOnline.setImageDrawable(getResources().getDrawable(R.drawable.offline));
            }else{
                imgIsOnline.setImageDrawable(getResources().getDrawable(R.drawable.online));
                if(shipdata == null){
                    getShip("default");
                }
            }
        }
    };

//    @Override
//    public void onPause() {
//        unregisterReceiver(networkStateReceiver);
//        super.onPause();
//    }


    private void getUser() {
        String user_data = getIntent().getExtras().getString("user_data");

        try {
            obj_user = new JSONObject(user_data);
            access_token = obj_user.getString("access_token");
            JSONObject user = obj_user.getJSONObject("user");
            user_id = user.getString("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getShip(final String type) {
        try {
            global.Loader(true);
            Api.Ship(user_id, access_token, new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) {
                    try {
                        JSONObject response = new JSONObject(result);
                        if (response.getBoolean("success")) {
                            if(type.equals("default")){
                                shipdata = response.getJSONArray("data");
                            }else{
                                sync_shipdata = response.getJSONArray("data");
                                prepareSyncItemShip();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        global.Toast("Error!");
                    }
                    global.Loader(false);
                }
                public void onErrorResponse(String result) {
                    try {
                        JSONObject response = new JSONObject(result);
                        global.Loader(false);
                        if(type.equals("default")){
                            global.Toast("GA Viewer is in offline mode.");
                            if(mydb.checkSync()){
                                String data =  mydb.getSync();
                                shipdata = new JSONArray(data);
                            }else{
                                shipdata = null;
                            }
                        }else{
                            sync_shipdata = null;
                            global.Alert(response.getString("message"));
                            if (sync_dialog != null){
                                sync_dialog.cancel();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void OpenSync(){
        is_sync = false;

        if (sync_dialog != null) sync_dialog = null;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the confirm_layout inflater
        LayoutInflater inflater = this.getLayoutInflater();
        Recycler_content = inflater.inflate(R.layout.sync_layout, null);
        builder.setView(Recycler_content);

        TextView tvOptionTitle = (TextView) Recycler_content.findViewById(R.id.tvOptionTitle);
        tvOptionTitle.setText("SELECT ITEMS TO SYNC");

        btnSyncCancel = (Button) Recycler_content.findViewById(R.id.btnSyncCancel);
        btnSync = (Button) Recycler_content.findViewById(R.id.btnSync);


        btnSyncCancel.setBackgroundResource(R.color.cancelBtn);
        btnSync.setBackgroundResource(R.color.colorNavBarText);

        btnSyncCancel.setOnClickListener(this);
        btnSync.setOnClickListener(this);

        recyclerView = (RecyclerView) Recycler_content.findViewById(R.id.recycler_view);
        iSyncAdapter = new SyncItemArrayAdapter(SyncItemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(iSyncAdapter);


        builder.create();
        sync_dialog = builder.create();
        sync_dialog.show();

        global.SetLoaderText = "Looking for ships...";
        getShip("sync");
    }

    private void prepareSyncItemShip() {
        if(sync_shipdata!=null){
            SyncItem SyncItem;
            for (int i = 0; i < sync_shipdata.length(); i++) {
                try {
                    String shipname = sync_shipdata.getJSONObject(i).getString("ship_name");
                    Integer shipcount = Integer.valueOf(sync_shipdata.getJSONObject(i).getString("count"));
                    String addcount = "";
                    if (shipcount > 0) {
                        addcount = " (" + shipcount + ")";
                    }
                    SyncItem = new SyncItem(shipname + addcount, null);
                    SyncItemList.add(SyncItem);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            iSyncAdapter.notifyDataSetChanged();
            SyncItemList = new ArrayList<SyncItem>();
        }
    }

    public void Sync(){

        if(iSyncAdapter.getActiveCount() > 0){

            btnSyncCancel.setBackgroundResource(R.color.defaultBtn);
            btnSync.setBackgroundResource(R.color.defaultBtn);

            SyncShipArray = new ArrayList<>();

            is_sync = true;
            mydb.insertSync(sync_shipdata);
            global.deleteFolder(directory);
            directory = cw.getDir("gaviewer", Context.MODE_PRIVATE);
            ArrayList<Boolean> listActive = iSyncAdapter.getListActive();
            sync_dialog.cancel();
            sync_dialog.setCancelable(false);

            TextView tvOptionTitle = (TextView) Recycler_content.findViewById(R.id.tvOptionTitle);
            tvOptionTitle.setText("SYNCING...");
            iSyncAdapter = new SyncItemArrayAdapter(SyncItemList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(iSyncAdapter);


//            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
//            {
//                @Override
//                public void onGlobalLayout()
//                {
//                    Syncing();
//                }
//            });


            sync_dialog.show();

            SyncItem SyncItem;
            for (int i = 0; i < sync_shipdata.length(); i++) {
                String shipname = null;

                try {
                    if(listActive.get(i)){
                        shipname = sync_shipdata.getJSONObject(i).getString("ship_name");
                        Integer shipcount = Integer.valueOf(sync_shipdata.getJSONObject(i).getString("count"));
                        SyncItem = new SyncItem(shipname, shipcount);
                        SyncItemList.add(SyncItem);
                        JSONObject SyncShipObjParts = new JSONObject();
                        SyncShipObjParts.put("name", shipname);
                        SyncShipObjParts.put("totalcount", shipcount);
                        SyncShipObjParts.put("downloadcount", 0);
                        SyncShipObjParts.put("position", i);
                        SyncShipArray.add(SyncShipObjParts);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            iSyncAdapter.notifyDataSetChanged();
            SyncItemList = new ArrayList<SyncItem>();
            SyncCount = 0;
            Syncing();

        }else{
            global.Toast("Please select a ship");
        }


    }

    private void Syncing(){
        JSONObject obj = (JSONObject) SyncShipArray.get(SyncCount);
        Integer synctotalcount = 0;
        try {
            synctotalcount = obj.getInt("totalcount");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if( synctotalcount.equals(0)){
            iSyncAdapter.setDone(SyncCount);
            SyncCount++;

            if(SyncCount < SyncShipArray.size()){
                Syncing();
            }else{
                closeSyncDialog();
            }
        }else{
            try {
                Integer position = obj.getInt("position");
                Integer totalcount = obj.getInt("totalcount");
                JSONObject shipdata = sync_shipdata.getJSONObject(position);
                JSONArray disciplinedata = shipdata.getJSONArray("disciplines");

                SyncDrawingData = new ArrayList();
                for (int i = 0; i <  disciplinedata.length(); i++) {
                    JSONObject disciplinearray = disciplinedata.getJSONObject(i);
                    JSONArray drawingdataArray = disciplinearray.getJSONArray("drawings");
                    for (int o = 0; o <  drawingdataArray.length(); o++) {
                        JSONObject drawingdata = drawingdataArray.getJSONObject(o);
                        SyncDrawingData.add(drawingdata.getString("ga_file_name"));
                    }
                }
                SyncDrawing(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public  void  SyncDrawing(final Integer pos){
            global.log(String.valueOf(pos));
            final Integer totalcount = SyncDrawingData.size();
            final String filename = String.valueOf(SyncDrawingData.get(pos));
            String filepath = directory.getAbsolutePath()+"/"+filename;
            if(!global.checkFile(filepath)){
                try {
                    Api.DownloadImage(filename, directory, new VolleyCallback() {
                        @Override
                        public void onSuccessResponse(String response) {
//                            Bitmap bitmap = global.StringToBitMap(response);
//                            String imgFile = saveImg(bitmap, filename);
                            if(pos < totalcount){

                                iSyncAdapter.addDownloadCount(SyncCount);
                                if((pos+1) >= totalcount){
                                    iSyncAdapter.setDone(SyncCount);
                                    SyncCount++;

                                    if(SyncCount < SyncShipArray.size()){
                                        Syncing();
                                    }else{
                                        closeSyncDialog();
                                    }
                                }else{
                                    SyncDrawing(pos+1);
                                }
                            }
                            iSyncAdapter.notifyDataSetChanged();
                        }
                        public void onErrorResponse(String result) {
                            try {
                                JSONObject response = new JSONObject(result);
                                global.Alert(response.getString("message"));
                                closeSyncDialog();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
    }


    private void closeSyncDialog(){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sync_dialog.cancel();
            }
        }, 2000);
    }

    private void setDrawing(Integer pos) {
        global.SetLoaderText = "Downloading Image...";

        try {
            if (selectedDrawing != pos) {
                tvDrawing.setText(drawingdata.getJSONObject(pos).getString("drawing_name"));
                selectedDrawing = pos;


                final String filename = drawingdata.getJSONObject(pos).getString("ga_file_name");
                String filepath = directory.getAbsolutePath()+"/"+filename;

                if(global.checkFile(filepath)){
                    setDownloadImage(filepath);
                }else{

                    global.LoaderWithCancel(new DialogCallback() {
                        @Override
                        public void onSuccessResponse(boolean result) {
                            CancelDownloadImage();
                        }
                    });


                    Api.DownloadImage(filename, directory, new VolleyCallback() {
                        @Override
                        public void onSuccessResponse(String response) {
                            setDownloadImage(response);
                            global.Loader(false);
                        }
                        public void onErrorResponse(String result) {
                            try {
                                JSONObject response = new JSONObject(result);
                                global.Alert(response.getString("message"));
                                global.Loader(false);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setDownloadImage(String imgFile){
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile);
        Bitmap scaled;
        if(myBitmap.getHeight()>phone_height){
            int nh = (int) ( myBitmap.getHeight() * (phone_height*1.0 / myBitmap.getWidth()) );
            scaled = Bitmap.createScaledBitmap(myBitmap, phone_height, nh, true);
        }else{
            scaled = myBitmap;
        }
        DrawingView.setImageBitmap(scaled);
        ImageContainer.setVisibility(View.VISIBLE);
        LnrDrawingLogoContainer.setVisibility(View.GONE);
    }



    private String saveImg(Bitmap bitmapImage, String fname){
        File mypath=new File(directory,fname);
        FileOutputStream fos = null;
        try {
            // fos = openFileOutput(filename, Context.MODE_PRIVATE);
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath()+"/"+fname;
    }



    private void SetOption(final String type) {
        if (dialog != null) dialog.cancel();
//        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this,R.style.AlertGAViewer));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the confirm_layout inflater
        LayoutInflater inflater = this.getLayoutInflater();
        View content = inflater.inflate(R.layout.option_dialog, null);
        builder.setView(content);

        TextView tvOptionTitle = (TextView) content.findViewById(R.id.tvOptionTitle);
        tvOptionTitle.setText("Select a " + type);

        recyclerView = (RecyclerView) content.findViewById(R.id.recycler_view);

        iAdapter = new ItemArrayAdapter(itemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(iAdapter);


        btnOptionCancel = (Button) content.findViewById(R.id.btnSyncCancelOption);
        btnOptionCancel.setOnClickListener(this);

        if (type.equals("Ship")) {
            prepareItemShip();
        } else if (type.equals("Discipline")) {
            prepareItemDiscipline();
        } else if (type.equals("Drawing")) {
            prepareItemDrawing();
        }



        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (type.equals("Ship")) {
                            setShip(position);
                        } else if (type.equals("Discipline")) {
                            setDiscipline(position);
                        } else if (type.equals("Drawing")) {
                            setDrawing(position);
                        }
                        dialog.cancel();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
//        builder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
        builder.create();
        dialog = builder.create();
        dialog.show();

    }

    private void setShip(Integer pos) {
        try {
            if (selectedShip != pos) {
                DrawingView.setImageBitmap(null);
                selectedShip = pos;
                JSONObject discipline = shipdata.getJSONObject(pos);
                disciplinedata = discipline.getJSONArray("disciplines");
                selectedDrawing = null;
                selectedDiscipline = null;
                tvDiscipline.setText(R.string.default_discipline);
                tvDrawing.setText(R.string.default_drawing);
                tvShip.setText(shipdata.getJSONObject(pos).getString("ship_name"));
                ImageContainer.setVisibility(View.GONE);
                LnrDrawingLogoContainer.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setDiscipline(Integer pos) {
        try {
            if (selectedDiscipline != pos) {
                DrawingView.setImageBitmap(null);
                selectedDiscipline = pos;
                JSONObject discipline = disciplinedata.getJSONObject(pos);
                drawingdata = discipline.getJSONArray("drawings");
                tvDiscipline.setText(disciplinedata.getJSONObject(pos).getString("discipline"));
                tvDrawing.setText(R.string.default_drawing);
                selectedDrawing = null;
                ImageContainer.setVisibility(View.GONE);
                LnrDrawingLogoContainer.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private void prepareItemShip() {
        if(shipdata!=null){
            Item item;
            for (int i = 0; i < shipdata.length(); i++) {
                try {
                    String shipname = shipdata.getJSONObject(i).getString("ship_name");
                    Integer shipcount = Integer.valueOf(shipdata.getJSONObject(i).getString("count"));
                    String addcount = "";
                    if (shipcount > 0) {
                        addcount = " (" + shipcount + ")";
                    }
                    String check = "";
                    if (selectedShip == null) {

                    } else if (selectedShip.equals(i)) {
                        check = "check";
                    }
                    item = new Item(shipname + addcount, check);
                    itemList.add(item);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            iAdapter.notifyDataSetChanged();
            itemList = new ArrayList<>();
        }
    }


    public void onBackPressed(){
//        global.Loader(false);
        global.Alert("test");
//        if(is_getImage()){
//            CancelDownloadImage();
//        }else{
////            finish();
//        }
    }

    private boolean is_getImage(){
        Boolean result = false;
        if(global.SetLoaderText.equals("Downloading Image...") && global.is_Loader()){
            result = true;
        }
        return result;
    }

    private void CancelDownloadImage(){
        Api.CancelCall();
        tvDrawing.setText(R.string.default_drawing);
        selectedDrawing = null;
    }



    public static JSONArray sortJsonArray(JSONArray array) throws JSONException {
        List<JSONObject> jsons = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            jsons.add(array.getJSONObject(i));
        }
        Collections.sort(jsons, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject lhs, JSONObject rhs) {
                String lid = null;
                String rid = null;
                try {
                    lid = lhs.getString("id");
                    rid = rhs.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Here you could parse string id to integer and then compare.
                return lid.compareTo(rid);
            }
        });
        return new JSONArray(jsons);
    }

    private void prepareItemDiscipline() {

        Item item;
        try {
            disciplinedata = sortJsonArray(disciplinedata);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < disciplinedata.length(); i++) {
            try {
                String disciplinename = disciplinedata.getJSONObject(i).getString("discipline");
                Integer disciplinecount = Integer.valueOf(disciplinedata.getJSONObject(i).getString("count"));
                String addcount = "";
                if (disciplinecount > 0) {
                    addcount = " (" + disciplinecount + ")";
                }
                String check = "";
                if (selectedDiscipline == null) {

                } else if (selectedDiscipline.equals(i)) {
                    check = "check";
                }
                item = new Item(disciplinename + addcount, check);
                itemList.add(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        iAdapter.notifyDataSetChanged();
        itemList = new ArrayList<>();
    }

    private void prepareItemDrawing() {

        Item item;
        for (int i = 0; i < drawingdata.length(); i++) {
            try {
                String drawingname = drawingdata.getJSONObject(i).getString("drawing_name");
                String check = "";
                if (selectedDrawing == null) {

                } else if (selectedDrawing.equals(i)) {
                    check = "check";
                }

                item = new Item(drawingname, check);
                itemList.add(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        iAdapter.notifyDataSetChanged();
        itemList = new ArrayList<>();
    }


    private void setListeners() {
        imgLogout.setOnClickListener(this);
        imgSync.setOnClickListener(this);
        tvShip.setOnClickListener(this);
        LnrDiscipline.setOnClickListener(this);
        LnrDrawing.setOnClickListener(this);

//        global.dialogloader.setOnKeyListener(new DialogInterface.OnKeyListener() {
//            @Override
//            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//                if(keyCode==KeyEvent.KEYCODE_BACK && !event.isCanceled()) {
//                    if(global.dialogloader.isShowing()) {
//                        //your logic here for back button pressed event
//                        Api.CancelCall();
//                        global.dialogloader.dismiss();
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgLogout:
                Logout();
                break;
            case R.id.btnSyncCancelOption:
                dialog.cancel();
                break;
            case R.id.imgSync:
                OpenSync();
                break;
            case R.id.btnSyncCancel:
                if(is_sync){
                    global.Toast("Please wait...");
                }else{
                    if(sync_dialog != null)
                    sync_dialog.cancel();
                }
                break;
            case R.id.btnSync:
                if(is_sync){
                    global.Toast("Please wait...");
                }else {
                    Sync();
                }
                break;
            case R.id.tvShip:
                SetOption("Ship");
                break;
            case R.id.LnrDiscipline:
                if(selectedShip != null){
                    SetOption("Discipline");
                }else{
                    global.Toast("Please select a ship");
                }
                break;
            case R.id.LnrDrawing:
                if(selectedDiscipline != null){
                    SetOption("Drawing");
                }else{
                    global.Toast("Please select a discipline");
                }
                break;
            default:
                break;
        }
    }


    public void Logout() {
        try {
            global.Confirm("Are you sure you want to logout?", new DialogCallback() {
                @Override
                public void onSuccessResponse(boolean result) {
                    if (result) {
                        mydb.deleteLogin();
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

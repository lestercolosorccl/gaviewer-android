package rcclpmo.com.gaviewer.controllers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import rcclpmo.com.gaviewer.callbacks.ResponseHandler;

/**
 * Created by 7567 on 07/03/2018.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, TextView.OnEditorActionListener, ResponseHandler {


    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }
}

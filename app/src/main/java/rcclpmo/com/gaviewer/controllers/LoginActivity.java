package rcclpmo.com.gaviewer.controllers;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rcclpmo.com.gaviewer.Api;
import rcclpmo.com.gaviewer.helpers.DBHelper;
import rcclpmo.com.gaviewer.helpers.GlobalHelper;
import rcclpmo.com.gaviewer.R;
import rcclpmo.com.gaviewer.callbacks.VolleyCallback;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>, OnClickListener, TextView.OnEditorActionListener {



    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */

    // UI references.
    private AutoCompleteTextView etEmail;
    private EditText etPassword;
    private Button btnLogin;
    private View mProgressView;
    private View mLoginFormView;
    private DBHelper mydb;
    private GlobalHelper global;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mydb = new DBHelper(this);
        global = new GlobalHelper(this);
        CheckLogin();
        // Set up the login form.
        etEmail = (AutoCompleteTextView) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);
        mLoginFormView = findViewById(R.id.login_form);
        btnLogin = (Button) findViewById(R.id.login);


        setListeners();

    }

    private void setListeners(){
        btnLogin.setOnClickListener(this);
        etPassword.setOnEditorActionListener(this);
        etEmail.setOnEditorActionListener(this);
    }



    @Override
    public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
            switch (textView.getId()){
                case R.id.email:

                    if(!isEmailValid(String.valueOf(etEmail.getText()))){
                        etEmail.requestFocus();
                    }else{
                        etPassword.requestFocus();
                    }
                    break;
                case R.id.password:
                    attemptLogin();
                    break;
                default:
                    break;
            }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login:
                    attemptLogin();
                break;
            default:
                break;
        }
    }


    public void onBackPressed(){
        if(is_showLoader()){
            CancelLogin();
        }else{
            finish();
        }
    }

    public void CancelLogin(){
        showLoader(false);
        global.Toast("Login cancelled");
        Api.CancelCall();
    }

    private void attemptLogin() {

        String email   = etEmail.getText().toString();
        String pass    = etPassword.getText().toString();

        if(is_showLoader()){
            global.Toast("Please wait...");
        }else if(!isEmailValid(email)){
            etEmail.requestFocus();
        }else if(!isPasswordValid(pass)){
            etPassword.requestFocus();
        }else {
            global.hideKeyboard(this);
            showLoader(true);
            try {
                Api.Login(email, pass, new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        try {
                            JSONObject response = new JSONObject(result);
                            if (response.getBoolean("success")) {
                                JSONObject data = response.getJSONObject("data");
                                SaveLogin(data);
                                CheckLogin();
                            }
                            global.Toast(response.getString("message"));
                            showLoader(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            global.Toast("Error!");
                            showLoader(false);
                        }
                    }
                    public void onErrorResponse(String result) {
                        try {
                            JSONObject response = new JSONObject(result);
                            global.Toast(response.getString("message"));
                            showLoader(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void CheckLogin(){
//        mydb.deleteLogin();
        if(mydb.checkLogin()){
            Object data = mydb.getLogin();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            i.putExtra("user_data", String.valueOf(data));
            startActivity(i);
            finish();
        }
    }

    private void SaveLogin(JSONObject data){
        mydb.insertLogin(data);
    }

    private void showLoader(boolean isLoading){
        btnLogin.setText(isLoading? "LOGGING IN...": "LOGIN");
    }

    private boolean is_showLoader(){
        Boolean result = false;
        if(btnLogin.getText().equals("LOGGING IN...")){
            result = true;
        }
        return result;
    }





    private boolean isEmailValid(String email) {
        Boolean result = true;
        if(TextUtils.isEmpty(email)){
            global.Toast("Email is required field.");
            result = false;
        }else{
            Pattern VALID_EMAIL_ADDRESS_REGEX =  Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
            if(!matcher.find()){
                global.Toast("Invalid Email format.");
                result = false;
            }
        }
        return result;
    }

    private boolean isPasswordValid(String password) {
        Boolean result = true;
        if(TextUtils.isEmpty(password)){
            global.Toast("Password is required field.");
            result = false;
        }else{
            if(password.length() < 4){
                global.Toast("Password is too short.");
                result = false;
            }
        }
        //TODO: Replace this with your own logic
        return result;
    }

    /**
     * Shows the progress UI and hides the login form.
     */


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        etEmail.setAdapter(adapter);
    }

    /**
     * Called when pointer capture is enabled or disabled for the current window.
     *
     * @param hasCapture True if the window has pointer capture.
     */
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

}

